#!/bin/bash
# Script pour créer un serveur DASH facilement
video_directory=$(pwd)

if [ -z $1 ]; then
    echo "Vous devez spécifier un fichier vidéo \"./create_dash_server.sh [video]\""
    exit 1
else
    video=$1
fi

if [ ! -e "$video" ]; then
    echo "Le fichier $video n'existe pas"
    exit 1
fi

#installe le serveur web (apache) et l'outil de conversion vidéo (ffmpeg)
sudo apt-get update
sudo apt-get install -y apache2 ffmpeg

cd /var/www/html;
sudo mkdir dash_test
sudo chown -R $LOGNAME:$LOGNAME dash_test
cd dash_test
mkdir media_files; cd media_files

#encodage de l'audio
ffmpeg -i "$video_directory"/"$video" -vn -acodec libvorbis -ab 128k -dash 1 my_audio.webm

#encodage de la vidéo : 2 passes pour chaque représentation
#90p
ffmpeg -i "$video_directory"/"$video" -c:v libvpx-vp9 -keyint_min 150 -g 150 -tile-columns 4 -frame-parallel 1  -f null -dash 1 \
-an -vf scale=160:90 -b:v 0 -crf 30 -pass 1 -dash 1 /dev/null && \
ffmpeg -i "$video_directory"/"$video" -c:v libvpx-vp9 -keyint_min 150 -g 150 -tile-columns 4 -frame-parallel 1  -f webm -dash 1 \
-an -vf scale=160:90 -b:v 0 -crf 30 -pass 2 -dash 1 video_160x90.webm
#180p
ffmpeg -i "$video_directory"/"$video" -c:v libvpx-vp9 -keyint_min 150 -g 150 -tile-columns 4 -frame-parallel 1  -f null -dash 1 \
-an -vf scale=320:180 -b:v 0 -crf 30 -pass 1 -dash 1 /dev/null && \
ffmpeg -i "$video_directory"/"$video" -c:v libvpx-vp9 -keyint_min 150 -g 150 -tile-columns 4 -frame-parallel 1  -f webm -dash 1 \
-an -vf scale=320:180 -b:v 0 -crf 30 -pass 2 -dash 1 video_320x180.webm
#360p
ffmpeg -i "$video_directory"/"$video" -c:v libvpx-vp9 -keyint_min 150 -g 150 -tile-columns 4 -frame-parallel 1  -f null -dash 1 \
-an -vf scale=640:360 -b:v 0 -crf 30 -pass 1 -dash 1 /dev/null && \
ffmpeg -i "$video_directory"/"$video" -c:v libvpx-vp9 -keyint_min 150 -g 150 -tile-columns 4 -frame-parallel 1  -f webm -dash 1 \
-an -vf scale=640:360 -b:v 0 -crf 30 -pass 2 -dash 1 video_640x360.webm
#720p
ffmpeg -i "$video_directory"/"$video" -c:v libvpx-vp9 -keyint_min 150 -g 150 -tile-columns 4 -frame-parallel 1  -f null -dash 1 \
-an -vf scale=1280:720 -b:v 0 -crf 30 -pass 1 -dash 1 /dev/null && \
ffmpeg -i "$video_directory"/"$video" -c:v libvpx-vp9 -keyint_min 150 -g 150 -tile-columns 4 -frame-parallel 1  -f webm -dash 1 \
-an -vf scale=1280:720 -b:v 0 -crf 30 -pass 2 -dash 1 video_1280x720.webm

#création du manifeste mpd
ffmpeg \
  -f webm_dash_manifest -i video_160x90.webm \
  -f webm_dash_manifest -i video_320x180.webm \
  -f webm_dash_manifest -i video_640x360.webm \
  -f webm_dash_manifest -i video_1280x720.webm \
  -f webm_dash_manifest -i my_audio.webm \
  -c copy \
  -map 0 -map 1 -map 2 -map 3 -map 4 \
  -f webm_dash_manifest \
  -adaptation_sets "id=0,streams=0,1,2,3 id=1,streams=4" \
  my_video_manifest.mpd

cd ..

#récupération de dash.js
wget http://cdn.dashjs.org/latest/dash.all.min.js

#création de index.html
cat > index.html <<EOL
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Test DASH</title>
    <style>
    video {
        width: 1280px;
        height: 720px;
    }
    </style>
</head>
<body>
    <video data-dashjs-player controls src="media_files/my_video_manifest.mpd"></video>
    <script src="dash.all.min.js"></script>
</body>
</html>
EOL
