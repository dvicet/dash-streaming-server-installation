create_dash_server.sh
-----

**Ce script permet d'installer facilement un serveur web permettant de lire une vidéo avec DASH.**

## Que fait ce script ?
* installe apache (serveur web) et ffmpeg (outil de conversion multimédia)
* convertit une vidéo dans différentes résolutions
* installe le lecteur vidéo [dash.js](https://github.com/Dash-Industry-Forum/dash.js)
* crée une page web (index.html) disponible à `http://[racine du serveur]/dash_test/`

## Installation
Le script fonctionne sur les distributions linux basées sur Debian et a été testé sous Ubuntu 20.

Pour récupérer le script, vous pouvez le télécharger :
```
wget "https://gitlab.com/dvicet/dash-streaming-server-installation/-/raw/master/create_dash_server.sh"
```
Ou vous pouvez cloner le dépôt :
```
git clone https://gitlab.com/dvicet/dash-streaming-server-installation.git
```

## Utilisation
Il suffit d'exécuter le script en donnant la vidéo que l'on souhaite convertir en argument :

```
./create_dash_server.sh [vidéo à convertir]
```

**Attention** : En fonction des capacités de votre ordinateur, la conversion vidéo peut être assez longue.

Une fois le script exécuté vous pourrez vous rendre à `http://[racine du serveur]/dash_test/` pour tester. Par exemple si votre serveur est en local : `http://localhost/dash_test/`
